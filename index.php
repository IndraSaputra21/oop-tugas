<?php
require 'animal.php';
require 'ape.php';
require 'frog.php';

$sheep = new Animal('shaun');

echo $sheep -> name.'<br>';
echo $sheep -> legs.'<br>';
echo $sheep -> coldBlooded ? 'true' : 'false'.'<br><br>';

$sungokong = new Ape('kera sakti');
echo $sungokong -> name.'<br>';
echo $sungokong -> legs.'<br>';
$sungokong -> yell();
echo '<br>';
echo $sheep -> coldBlooded ? 'true' : 'false'.'<br><br>';


$kodok = new Frog('buduk');
echo $kodok -> name.'<br>';
$kodok -> jump();
echo '<br>';
echo $kodok -> legs.'<br>';
echo $sheep -> coldBlooded ? 'true' : 'false'.'<br><br>';
?>