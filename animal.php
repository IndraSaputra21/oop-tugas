<?php

class Animal {
    public $name;
    public $legs = 2;
    public $coldBlooded = false;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function _setName($name) {
        $this->name = $name;
    }
    public function _setLegs($legs) {
        $this->legs = $legs;
    }

    public function _getName(){
        return $this -> name;
    }
}

?>